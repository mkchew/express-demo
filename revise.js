const Joi = require('joi');
const express = require('express');
const app = express();


app.use(express.json());

// Data
const courses = [
    {'id': 1, 'name': 'course1'},
    {'id': 2, 'name': 'course2'},
    {'id': 3, 'name': 'course3'}
];

// Home page
app.get('/', (req, res) => {
    res.send('Hello World');
});

app.get('/api/courses', (req, res) => {
    res.send(courses);
});

app.get('/api/courses/:id', (req, res) => {
    // Error 404 Not Found
    let course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send(`Course with the given ID ${req.params.id} is not found`);

    res.send(course);

});

app.post('/api/courses', (req, res) => {   
    //Important
    //Error 400 Bad Request
    const {error} = dataValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    const course = {
        'id': courses.length + 1,
        'name': req.body.name
    };

    courses.push(course);
    res.send(courses);

});

app.put('/api/courses/:id', (req, res) => {
    //Check if the ID provided exists
    let course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send(`Course with the given ID ${req.params.id} is not found`);
    
    //Check if the updated field is valid
    const {error} = dataValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    course.name = req.body.name; 
    res.send(courses);
    
});

function dataValidation(course){
    const schema = {
        name: Joi.string().min(3).required()
    };
    return Joi.validate(course, schema)
};

app.delete('/api/courses/:id', (req, res) => {
    //Check if the ID provided exists
    let course = courses.find(c => c.id === parseInt(req.params.id));
    //if (!course) return res.status(404).send(`Course with the given ID ${req.params.id} is not found`);
    console.log('ID Available');

    const index = courses.indexOf(course);
    courses.splice(index, 1);

    res.send(courses);
});


const port = process.env.PORT || 3000;
app.listen(port, ()=> console.log(`Listen to Port ${port}....`));
